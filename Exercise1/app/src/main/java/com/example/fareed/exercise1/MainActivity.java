package com.example.fareed.exercise1;

import android.app.AlertDialog;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.widget.EditText;
import android.widget.TextView;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    //constants
    private static final String LOAN_TOTAL = "LOAN_TOTAL";
    private static final String INTEREST_RATE = "INTEREST_RATE";

    private double currentLoanTotal; //loan amount entered by the user
    private double interestRate; //interest amount entered by the user

    //5 years
    private EditText fiveEditText; //displays 5 years
    private EditText loanTotal5EditText; //displays the total with 5 years
   // private EditText fiveEditText01;
    private double fiveYearTotal;
    //10 years
    private EditText tenEditText; //displays 10 years
    private EditText loanTotal10EditText; //displays the total with 10 years
    //private EditText fiveEditText02;
    private double tenYearTotal;
    //15 years
    private EditText fifteenEditText; //displays 15 years
    private EditText loanTotal15EditText; //displays the total with 15
    //private EditText fiveEditText03;
    private double fifteenYearTotal;
    // 20 years
    private EditText twentyEditText; //displays 20 years
    private EditText loanTotal20EditText; //displays the total with 20 years
    //private EditText fiveEditText04;
    private double twentyYearTotal;
    //25 years
    private EditText twentyFiveEditText; //displays 25 years
    private EditText loanTotal25EditText; //displays the total with 25 years
    //private EditText fiveEditText05;
    private double twentyFiveYearTotal;
    //30 years
    private EditText thirtyEditText; //displays 30 years
    private EditText loanTotal30EditText; //displays the total with 30 years
    //private EditText fiveEditText06;
    private double thirtyYearTotal;

    public EditText loanEditText; //accepts user input for loan total
    public EditText interestRateEditText; //accepts user input for interestRate


    NumberFormat nm = NumberFormat.getNumberInstance();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //check if app just started or being restored from memory
        if (savedInstanceState == null) // the app just started
        {
            //initialize some fields
            currentLoanTotal = 0;
            interestRate = 0;
        } else // being restored from memory
        {
            //initialize fields from saved values
            currentLoanTotal = savedInstanceState.getDouble(LOAN_TOTAL);
            interestRate = savedInstanceState.getDouble(INTEREST_RATE);
        }
        //get references to all the items on the gui
        //5
        fiveEditText = (EditText) findViewById(R.id.fiveEditText);
        //loanTotal5EditText = (EditText) findViewById(R.id.fiveEditText01);
        //10
        tenEditText = (EditText) findViewById(R.id.tenEditText);
       // loanTotal10EditText = (EditText) findViewById(R.id.tenEditText02);
        //15
        fifteenEditText = (EditText) findViewById(R.id.fifteenEditText);
        //loanTotal15EditText = (EditText) findViewById(R.id.fifteenEditText03);
        //20
        twentyEditText = (EditText) findViewById(R.id.twentyEditText);
        //loanTotal20EditText = (EditText) findViewById(R.id.twentyEditText04);
        //25
        twentyFiveEditText = (EditText) findViewById(R.id.twentyFiveEditText);
        //loanTotal25EditText = (EditText) findViewById(R.id.twentyFiveEditText05);
        //30
        thirtyEditText = (EditText) findViewById(R.id.thirtyEditText);
      //  loanTotal30EditText = (EditText) findViewById(R.id.thirtyEditText);


        loanEditText = (EditText) findViewById(R.id.loanEditText);
        interestRateEditText = (EditText) findViewById(R.id.annualInterestRateEditText);
        //handle when the rate changes

        interestRateEditText.addTextChangedListener(InterestRateEditWatcher);

    }

    // update the 5, 10, 15,20, 25 and 30 editTexts
    private void updateStandard() {
        double fivePow = 5*12;
        double tenPow = 10*12;
        double fifteenPow = 15*12;
        double twentyPow =20*12;
        double twentyFivePow =25*12;
        double thirtyPow = 30*12;


        //calculate 5total
        //EMI = ( P × r × (1+r)n ) / ((1+r)n − 1)
        //Log.e("&&&&&&",""+(currentLoanTotal * (interestRate/12.0)) );
        double fiveYearLoan = (currentLoanTotal * (interestRate/12)*(Math.pow((interestRate/12)+1, fivePow))) / (Math.pow((interestRate/12)+1, fivePow)-1);
        fiveYearTotal = (fiveYearLoan * fivePow);
        //set the corresponding editText Value
        fiveEditText.setText(nm.format(fiveYearLoan));
        //loanTotal5EditText.setText(nm.format(fiveYearTotal));

        //calculate 10total
        double tenYearLoan = (currentLoanTotal * (interestRate/12)*(Math.pow((interestRate/12)+1, tenPow))) / (Math.pow((interestRate/12)+1, tenPow)-1);
        tenYearTotal = (tenYearLoan * (10*12));
        //set the corresponding editText Value
        tenEditText.setText(nm.format(tenYearLoan));
        //loanTotal10EditText.setText(nm.format(tenYearTotal));

        //calculate 15total
        double fifteenYearLoan = (currentLoanTotal * (interestRate/12)*(Math.pow((interestRate/12)+1, fifteenPow))) / (Math.pow((interestRate/12)+1, fifteenPow)-1);
        fifteenYearTotal = (fifteenYearLoan * fifteenPow);
        //set the corresponding editText Value
        fifteenEditText.setText(nm.format(fifteenYearLoan));
        //loanTotal15EditText.setText(nm.format(fifteenYearTotal));

        //calculate 20 total
        double twentyYearLoan = (currentLoanTotal * (interestRate/12)*(Math.pow((interestRate/12)+1, twentyPow))) / (Math.pow((interestRate/12)+1, twentyPow)-1);
        twentyYearTotal = (fiveYearLoan * twentyPow);
        //set the corresponding editText Value
        twentyEditText.setText(nm.format(twentyYearLoan));
        //loanTotal20EditText.setText(nm.format(twentyYearTotal));

        //calculate 25 total
        double twentyFiveYearLoan = (currentLoanTotal * (interestRate/12)*(Math.pow((interestRate/12)+1, twentyFivePow))) / (Math.pow((interestRate/12)+1, twentyFivePow)-1);
        twentyFiveYearTotal = (twentyFiveYearLoan * twentyFivePow);
        //set the corresponding editText Value
        twentyFiveEditText.setText(nm.format(twentyFiveYearLoan));
        //loanTotal25EditText.setText(nm.format(twentyFiveYearTotal));

        //calculate 30total
        double thirtyYearLoan = (currentLoanTotal * (interestRate/12)*(Math.pow((interestRate/12)+1, thirtyPow))) / (Math.pow((interestRate/12)+1, thirtyPow)-1);
        thirtyYearTotal = (thirtyYearLoan * thirtyPow);
        //set the corresponding editText Value
        thirtyEditText.setText(nm.format(thirtyYearLoan));
        //loanTotal30EditText.setText(nm.format(thirtyYearTotal));
    }

    private final TextWatcher InterestRateEditWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //interestRateEditText.setText("0.0");
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (!(Double.parseDouble(loanEditText.getText().toString()) == 0.0)) {
                 currentLoanTotal = Double.parseDouble(loanEditText.getText().toString());
                interestRate = Double.parseDouble(interestRateEditText.getText().toString());
                /**
                 fiveEditText.setText(nm.format(fiveYearTotal));
                 tenEditText.setText(nm.format(tenYearTotal));
                 fifteenEditText.setText(nm.format(fifteenYearTotal));
                 twentyEditText.setText(nm.format(twentyYearTotal));
                 twentyFiveEditText.setText(nm.format(twentyFiveYearTotal));
                 thirtyEditText.setText(nm.format(thirtyYearTotal));**/
                updateStandard();

            } else {
                alert();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {


        }
    };

    public void alert(){
        AlertDialog.Builder alertMsg = new AlertDialog.Builder(this);
        alertMsg.setMessage("Please fill out Loan Amount first.").create();
        alertMsg.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putDouble(LOAN_TOTAL, currentLoanTotal);
    }
}
